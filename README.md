# Artifacts
This is the repository for group **Three** in the course **Large Systems Development**.

---

[Logical data model](<./Logical Data Model.pdf>)

[Use case diagram](<./Use case diagram/Use case diagram v3.png>)

[Use case descriptions](<./Use case descriptions/Use case descriptions v3.pdf>)

[Sequence diagrams](<./Sequence diagrams>)
